﻿using System;
using Cnam.TestValidation.Model;
using Xunit;


[assembly: CollectionBehavior(CollectionBehavior.CollectionPerAssembly)]
namespace Cnam.TestValidation.Testing._1_ComponentTest
{
    [Trait("Category", "Periodical Rate")]
    public class ModelCalculationDuration
    {
        [Fact(DisplayName = "Periodicale rate number from standard number")]
        public void CorrectPeriodicalRateCalculationInStandardNumber()
        {
            PeriodicaleRate periodicalRate = new PeriodicaleRate(new Rate(0.0205d, TypeRate.StandardNumber));
            Assert.Equal(0.00170833d, periodicalRate.CalculatePeriodicalRateInStandardNumber(), 4);
        }

        [Fact(DisplayName = "Periodicale rate number from percent number")]
        public void CorrectPeriodicalRateCalculationInPercentNumber()
        {
            PeriodicaleRate periodicalRate = new PeriodicaleRate(new Rate(2.05d, TypeRate.Percent));
            Assert.Equal(0.00170833d, periodicalRate.CalculatePeriodicalRateInStandardNumber(), 4);
        }


        [Fact(DisplayName = "Periodicale rate percent from standard number")]
        public void CorrectPeriodicalRateCalculationPercentInStandardNumber()
        {
            PeriodicaleRate periodicalRate = new PeriodicaleRate(new Rate(0.0205d, TypeRate.StandardNumber));
            Assert.Equal(0.170833d, periodicalRate.CalculatePeriodicalRateInPercent(), 3);
        }

        [Fact(DisplayName = "Periodicale rate percent from percent number")]
        public void CorrectPeriodicalRateCalculationPercentInPercentNumber()
        {
            PeriodicaleRate periodicalRate = new PeriodicaleRate(new Rate(2.05d, TypeRate.Percent));
            Assert.Equal(0.170833d, periodicalRate.CalculatePeriodicalRateInPercent(), 3);
        }
    }
        
    [Trait("Category", "Calculation Date")]
    public class ModelCalculationDueDate
    {
        [Theory(DisplayName = "Due date number")]
        [InlineData(10)]
        [InlineData(100)]
        public void TotalNumberOfDueDate(int year)
        {
            HomeLoanDueDate homeLoanDate = new HomeLoanDueDate(year);
            Assert.Equal(year * homeLoanDate.GetNumberDueDateInOneYear(), homeLoanDate.GetNumberOfDueDate());
        }

        [Theory(DisplayName = "Due date number with deferred payment")]
        [InlineData(10,1,12)]
        [InlineData(3, 5,35)]
        public void TotalNumberOfDueDateWithDefferredPayement(int year,int differedYear,int expected)
        {
            HomeLoanDueDate homeLoanDate = new HomeLoanDueDate(year, differedYear);
            Assert.Equal(expected, homeLoanDate.GetNumberDueDateDiffered());

        }

        [Theory(DisplayName = "Due date number with deferred payment")]
        [InlineData(10, 1,108)]
        [InlineData(3, 5,1)]
        public void TotalNumberOfDueDateWithAmortization(int year, int differedYear, int expected)
        {
            HomeLoanDueDate homeLoanDate = new HomeLoanDueDate(year, differedYear);
            Assert.Equal(expected, homeLoanDate.GetNumberOfAmortization());
        }

        [Theory(DisplayName = "Error on due date number")]
        [InlineData(-1)]
        [InlineData(0)]
        public void TotalNumberOfDueDateWhenError(int year)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new HomeLoanDueDate(year));
        }
        [Theory(DisplayName = "Error on due date number with deferred payment")]
        [InlineData(-1, -1)]
        [InlineData(-1, 2)]
        [InlineData(2, -1)]
        public void TotalNumberOfDueDateWithDefferredPayementWhenError(int year, int differedYear)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new HomeLoanDueDate(year, differedYear));
        }
    }
}
