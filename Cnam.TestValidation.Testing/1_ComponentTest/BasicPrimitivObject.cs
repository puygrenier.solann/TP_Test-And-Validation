﻿using System;
using System.Collections.Generic;
using Cnam.TestValidation.Model;
using Xunit;

namespace Cnam.TestValidation.Testing._1_ComponentTest
{
    [Trait("Category", "Money")]
    public class BasicPrimitivObject
    {
        
        [Theory(DisplayName = "Money + Money")]
        [MemberData(nameof(DataTestMoneyPlus))]
        public void VerifyMoneyBehaviorOnPlusOperation(Money moneyA, Money moneyB, Money moneyExpected)
        {
            Money temporaryValue = new Money(currency: moneyA.Currency, value:0) ;
            temporaryValue = moneyA + moneyB;
            Assert.Equal(moneyExpected, temporaryValue);
        }

        [Theory(DisplayName = "Money - Money")]
        [MemberData(nameof(DataTestMoneyLess))]
        public void VerifyMoneyBehaviorOnLessOperation(Money moneyA, Money moneyB, Money moneyExpected)
        {
            Money temporaryValue = new Money(0, moneyA.Currency);
            temporaryValue = moneyA - moneyB;
            Assert.Equal(moneyExpected, temporaryValue);
        }

        [Theory(DisplayName = "ERROR = Money € + Money $")]
        [MemberData(nameof(DataTestMoneyOperationError))]
        public void VerifyMoneyBehaviorOnOperationError(Money moneyA, Money moneyB)
        {
            Assert.Throws<InvalidOperationException>(() => moneyA + moneyB);
        }

        [Theory(DisplayName = "ERROR = 1.251")]
        [InlineData(0.125f)]
        public void VerifyMoneyBehaviorOnErrorInstantiation(decimal unhandledMoney)
        {
            Assert.Throws<ArgumentException>(() => new Money(unhandledMoney,'€'));
        }


        public static IEnumerable<object[]> DataTestMoneyPlus
        {
            get
            {
                yield return new object[]
                {
                    new Money(10,'€'),
                    new Money(15,'€'),
                    new Money(25,'€')
                };
                yield return new object[]
                {
                    new Money(15.45m,'€'),
                    new Money(25.55m,'€'),
                    new Money(41,'€')
                };
            }
        }

        public static IEnumerable<object[]> DataTestMoneyLess
        {
            get
            {
                yield return new object[]
                {
                    new Money(50,'€'),
                    new Money(15.5m,'€'),
                    new Money(34.5m,'€'),
                };
                yield return new object[]
                {
                    new Money(10,'€'),
                    new Money(20,'€'),
                    new Money(-10,'€')
                };
            }
        }

        public static IEnumerable<object[]> DataTestMoneyOperationError
        {
            get
            {
                yield return new object[]
                {
                    new Money(50,'€'),
                    new Money(50,'$'),
                };
            }
        }
    }
}
