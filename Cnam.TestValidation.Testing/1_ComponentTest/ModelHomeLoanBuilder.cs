﻿using Xunit;
using Cnam.TestValidation.Model;

namespace Cnam.TestValidation.Testing._1_ComponentTest
{
    [Trait("Category", "Builder Home Loan")]
    public class ModelHomeLoanBuilder
    {
        [Fact(DisplayName ="Creation object")]
        public void CreateLoanUsingBuilder()
        {

            HomeLoanRequestBuilder homeLoanBuilder = new ();
            homeLoanBuilder.Reset();

            homeLoanBuilder.Currency = '€';
            homeLoanBuilder.SetRateInStandardNumber();

            homeLoanBuilder.RequestedMoney = 100_000m;
            homeLoanBuilder.RequestedYear = 10;
            homeLoanBuilder.DifferedYear = 1;

            homeLoanBuilder.InterestRate =0.0205f;
            homeLoanBuilder.AssuranceRate = 0.0018f;

            Assert.True(homeLoanBuilder.CanBeInstantiate());
                
            HomeLoan homeLoan = homeLoanBuilder.Build();

            Assert.Equal(11643.59m, homeLoan.GetTotalInterestCost().Value);
            Assert.Equal(1800, homeLoan.GetTotalAssuranceCost().Value);
            Assert.Equal(13443.59m, homeLoan.GetTotalCost().Value);
        }
        [Fact(DisplayName ="Error on loan creation")]
        public void ErrorOnHomeLoanCreation()
        {
            HomeLoanRequestBuilder homeLoanBuilder = new HomeLoanRequestBuilder();
            Assert.Throws<InvalideHomeLoanException>(() => homeLoanBuilder.Build());
        }
    }
}
