﻿using Cnam.TestValidation.Model;
using System;
using System.Collections.Generic;
using Xunit;

namespace Cnam.TestValidation.Testing._1_ComponentTest
{
    [Trait("Category", "Home Loan")]
    public class ModelComplexeObject
    {
        [Theory(DisplayName = "Verify correct behavior of HomeLoan for montly value")]
        [MemberData(nameof(ValideMonthlyCalculationForHomeLoan))]
        public void TestOfHomeLoan(HomeLoan homeLoan, Money expectedMonthlyCostWithoutAssurance, Money expecteMonthlydAssuranceCost)
        {
            Assert.Equal(expectedMonthlyCostWithoutAssurance, homeLoan.GetMonthlyCostWithoutAssurance());
            Assert.Equal(expecteMonthlydAssuranceCost, homeLoan.GetMonthlyAssuranceCost());
        }

        [Theory(DisplayName = "Verify correct behavior of HomeLoan for total value")]
        [MemberData(nameof(ValideCalculationForHomeLoan))]
        public void TestMensualityOfHomeLoan(HomeLoan homeLoan, Money expectedInterest,Money expecteAssuranceCost,Money expectedTotalCostForLoan)
        {
            Assert.Equal(expectedInterest, homeLoan.GetTotalInterestCost());
            Assert.Equal(expecteAssuranceCost, homeLoan.GetTotalAssuranceCost());
            Assert.Equal(expectedTotalCostForLoan, homeLoan.GetTotalCost());
        }

        [Fact(DisplayName ="Verify loan data after several payement")]
        public void TestForInterestAfterDuration()
        {
            int numberOdDueDate = 5;
            HomeLoan SystemUnderTest = new HomeLoan(
                        new Money(100_000, '€'),
                        new Rate(2.05f, TypeRate.Percent),
                        10,
                        0,
                        new Rate(0.18f, TypeRate.Percent)
                    );
            HomeLoanDueDateDetail value = SystemUnderTest.GetLoanDataAfterDueDate(numberOdDueDate);
            Assert.Equal(new Money(165.68m, '€'), value.Interest);
            Assert.Equal(new Money(756.70m, '€'), value.Principale);
            Assert.Equal(new Money(15, '€'), value.Assurance);
            Assert.Equal(new Money(937.38m, '€'), value.TotalCostAtDueDate);
        }
        

        [Theory(DisplayName = "Verify Home Loan on Error")]
        [InlineData(-1    , 2.05f  ,10  ,0  ,0.18f)]
        [InlineData(100_000, 2.05f  , 0  ,0  ,0.18f)]
        [InlineData(100_000, 2.05f  ,-1  ,0  ,0.18f)]
        [InlineData(100_000, 2.05f  , 10  ,-1  ,0.18f)]
        public void TestMensualityOfHomeLoanOnError(decimal askedMoney, double annualRate, int yearDurration, int defferedYear,double assuranceRate)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new HomeLoan(new Money(askedMoney, '€'), new Rate(annualRate, TypeRate.Percent), yearDurration, defferedYear, new Rate(assuranceRate, TypeRate.Percent)));
        }

        public static IEnumerable<object[]> ValideMonthlyCalculationForHomeLoan
        {
            get
            {
                yield return new object[]
                {
                    new HomeLoan(
                        new Money(100000, '€'), 
                        new Rate(2.05f, TypeRate.Percent),
                        10,
                        0, 
                        new Rate(0.18f, TypeRate.Percent)
                    ),
                    new Money(922.38m, '€'), // expected Monthly Cost Without Assurance
                    new Money(15m, '€') // expected Assurance Cost
                };
                yield return new object[]
                {
                    new HomeLoan(
                        new Money(100000, '€'),
                        new Rate(2.05f, TypeRate.Percent),
                        10,
                        5,
                        new Rate(0.18f, TypeRate.Percent)
                    ),
                    new Money(1754.96m, '€'), // expected Monthly Cost Without Assurance
                    new Money(15m, '€') // expected Assurance Cost
                };
                yield return new object[]
                {
                    new HomeLoan(
                        new Money(100000, '€'),
                        new Rate(2.05f, TypeRate.Percent),
                        5,
                        10,
                        new Rate(0.18f, TypeRate.Percent)
                    ),
                    new Money(100170.83m, '€'), // expected Monthly Cost Without Assurance
                    new Money(15m, '€') // expected Assurance Cost
                };
                yield return new object[]
                {
                    new HomeLoan(
                        new Money(0, '€'),
                        new Rate(2.05f, TypeRate.Percent),
                        10,
                        0,
                        new Rate(0.18f, TypeRate.Percent)
                    ),
                    new Money(0, '€'), // expected Monthly Cost Without Assurance
                    new Money(0, '€') // expected Assurance Cost
                };
            }
        }
        public static IEnumerable<object[]> ValideCalculationForHomeLoan
        {
            get
            {
                yield return new object[]
                {
                    new HomeLoan(
                        new Money(100_000, '€'),
                        new Rate(2.05f, TypeRate.Percent),
                        10,
                        0,
                        new Rate(0.18f, TypeRate.Percent)
                    ),
                    new Money(10_685.06m, '€'), // expected Interest
                    new Money(1_800, '€'), // expected Assurance Cost
                    new Money(12_485.06m, '€') // expected Total Cost
                };
                yield return new object[]
                {
                    new HomeLoan(
                        new Money(10_0000, '€'),
                        new Rate(2.05f, TypeRate.Percent),
                        10,
                        5,
                        new Rate(0.18f, TypeRate.Percent)
                    ),
                    new Money(15_547.61m, '€'), // expected Interest
                    new Money(1_800m, '€'), // expected Assurance Cost
                    new Money(17_347.61m, '€') // expected Total Cost
                };
                yield return new object[]
                {
                    new HomeLoan(
                        new Money(10_0000, '€'),
                        new Rate(2.05f, TypeRate.Percent),
                        5,
                        10,
                        new Rate(0.18f, TypeRate.Percent)
                    ),
                    new Money(10_249.80m, '€'), // expected Interest
                    new Money(900, '€'), // expected Assurance Cost
                    new Money(11_149.80m, '€') // expected Total Cost
                };
                yield return new object[]
                {
                    new HomeLoan(
                        new Money(0, '€'),
                        new Rate(2.05f, TypeRate.Percent),
                        10,
                        0,
                        new Rate(0.18f, TypeRate.Percent)
                    ),
                    new Money(0, '€'), // expected Interest
                    new Money(0, '€'), // expected Assurance Cost
                    new Money(0, '€') // expected Total Cost
                };
            }
        }
    }
}
