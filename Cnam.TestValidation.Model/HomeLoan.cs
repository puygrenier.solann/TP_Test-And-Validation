﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.Model;
public class HomeLoan
{
    private Money _requestedMoney;
    private Rate _annualRate;
    private int _yearDuration;
    private int _deferredYear;
    private Rate _assuranceRate;

    private PeriodicaleRate _periodicaleRate;
    private HomeLoanDueDate _homeLoanDueDate;

    public Money RequestedMoney { get { return _requestedMoney; } }
    public Rate AnnualRate { get { return _annualRate; } }
    public int YearDuration { get { return _yearDuration; } }
    public int DefferedYear { get { return _deferredYear; } }
    public Rate AssuranceRate { get { return _assuranceRate; } }

    public HomeLoan(Money requestedMoney, Rate annualRate, int yearDuration, int deferredYear, Rate assuranceRate)
    {
        if (requestedMoney.Value < 0) throw new ArgumentOutOfRangeException(nameof(requestedMoney) + " cannot be negativ");
        _requestedMoney = requestedMoney;
        _annualRate = annualRate;
        _yearDuration = yearDuration;
        _deferredYear = deferredYear;
        _assuranceRate = assuranceRate;
        _periodicaleRate = new PeriodicaleRate(annualRate);
        _homeLoanDueDate = new HomeLoanDueDate(yearDuration, deferredYear);
    }

    public Money GetMonthlyCostWithoutAssurance()
    {
        return _requestedMoney *
            (decimal)(_periodicaleRate.CalculatePeriodicalRateInStandardNumber() / (1 - Math.Pow(1 + _periodicaleRate.CalculatePeriodicalRateInStandardNumber(), -_homeLoanDueDate.GetNumberOfAmortization())));
    }

    public Money GetMonthlyAssuranceCost()
    {
        return _requestedMoney * (decimal)(_assuranceRate.GetValueAsStandardNumber() / _homeLoanDueDate.NumberDueDatePerYear);
    }

    public Money GetTotalInterestCost()
    {
        Money totalInterest = new Money(0, _requestedMoney.Currency);
        Money remainingMoneyToPay = _requestedMoney;
        Money InterestforThisDueDate, PrincipalToPayForThisDueDate;

        for (int i = 0; i < _homeLoanDueDate.GetNumberOfDueDate(); i++)
        {
            if (i < _homeLoanDueDate.GetNumberDueDateDiffered())
            {
                totalInterest = totalInterest + CalculateInterest(remainingMoneyToPay);
            }
            else
            {
                InterestforThisDueDate = CalculateInterest(remainingMoneyToPay);
                PrincipalToPayForThisDueDate = GetMonthlyCostWithoutAssurance() - InterestforThisDueDate;

                remainingMoneyToPay = remainingMoneyToPay - PrincipalToPayForThisDueDate;
                totalInterest = totalInterest + InterestforThisDueDate;
            }
        }
        return totalInterest;
    }
    public Money GetTotalAssuranceCost()
    {
        Money totalAssurance = new Money(0, _requestedMoney.Currency);
        for (int i = 0; i < _homeLoanDueDate.GetNumberOfDueDate(); i++)
        {
            totalAssurance = totalAssurance + GetMonthlyAssuranceCost();
        }
        return totalAssurance;
    }

    public Money GetTotalCost()
    {
        return GetTotalInterestCost() + GetTotalAssuranceCost();
    }

    public HomeLoanDueDateDetail GetLoanDataAfterDueDate(int numberOfDueDate)
    {
        Money totalInterest = new Money(0, _requestedMoney.Currency);
        Money remainingMoneyToPay = _requestedMoney;

        Money InterestforThisDueDate = CalculateInterest(remainingMoneyToPay);
        Money PrincipalToPayForThisDueDate = new Money(0, _requestedMoney.Currency);

        for (int i = 0; i < numberOfDueDate; i++)
        {
            if (i < _homeLoanDueDate.GetNumberDueDateDiffered())
            {
                totalInterest = totalInterest + CalculateInterest(remainingMoneyToPay);
            }
            else
            {
                InterestforThisDueDate = CalculateInterest(remainingMoneyToPay);
                PrincipalToPayForThisDueDate = GetMonthlyCostWithoutAssurance() - InterestforThisDueDate;

                remainingMoneyToPay = remainingMoneyToPay - PrincipalToPayForThisDueDate;

            }
        }

        Money TotalAtDueDate = InterestforThisDueDate + PrincipalToPayForThisDueDate + GetMonthlyAssuranceCost();
        return new HomeLoanDueDateDetail(numberOfDueDate, remainingMoneyToPay, InterestforThisDueDate, PrincipalToPayForThisDueDate, GetMonthlyAssuranceCost(), TotalAtDueDate);
    }
    private Money CalculateInterest(Money remainingMoneyToPay)
    {
        return remainingMoneyToPay * _periodicaleRate.CalculatePeriodicalRateInStandardNumber();
    }
}
