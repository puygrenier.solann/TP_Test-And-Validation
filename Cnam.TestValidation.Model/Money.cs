﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.Model;
public class Money : IEquatable<Money>
{
    private readonly decimal _value;
    private readonly char _currency;

    public char Currency { get; }
    public decimal Value { get; }

    public Money(decimal value, char currency)
    {
        if (Decimal.Round(value, 2) != value) throw new ArgumentException("Money value cannot have more than 2 digits place");
        _value = value;
        Value = _value;
        _currency = currency;
        Currency = _currency;
    }

    public static Money operator +(Money a) => a;
    public static Money operator -(Money a) => new Money(-a._value, a._currency);
    public static Money operator +(Money a, Money b)
    {
        if (a._currency == b._currency) return new Money(a._value + b._value, a._currency);
        else throw new InvalidOperationException("Cannot add two different currency");
    }
    public static Money operator -(Money a, Money b)
    {
        if (a._currency == b._currency) return new Money(a._value - b._value, a._currency);
        else throw new InvalidOperationException("Cannot substract two different currency");
    }
    public static Money operator *(Money a, int b) { return new Money(a._value * b, a.Currency); }
    public static Money operator *(Money a, decimal b) { return new Money(Decimal.Round(a._value * b, 2), a.Currency); }
    public static Money operator *(Money a, double b) { return new Money(Decimal.Round(a._value * (decimal)b, 2), a.Currency); }
    public static bool operator >(Money a, Money b)
    {
        if (a._currency != b._currency) throw new InvalidOperationException("Cannot add two different currency");
        else return (a._value > b._value);
    }
    public static bool operator <(Money a, Money b)
    {
        if (a._currency != b._currency) throw new InvalidOperationException("Cannot add two different currency");
        else return (a._value > b._value);
    }
    public static bool operator ==(Money a, Money b)
    {
        if (a._currency != b._currency) throw new InvalidOperationException("Cannot add two different currency");
        else return (a._value == b._value);
    }
    public static bool operator !=(Money a, Money b)
    {
        if (a._currency != b._currency) throw new InvalidOperationException("Cannot add two different currency");
        else return (a._value != b._value);
    }

    public override string ToString()
    {
        return $"{_value}{_currency}";
    }

    public bool Equals(Money? other)
    {
        if (other is null) return false;
        return ((this._value == other._value) && this._currency == other._currency);
    }
}

