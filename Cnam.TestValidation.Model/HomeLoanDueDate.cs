﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.Model;

public class HomeLoanDueDate
{
    private const int _numberDueDatePerYear = 12;
    private int _year;
    private int _differedYear;

    public HomeLoanDueDate(int year) : this(year, 0) { }

    public HomeLoanDueDate(int year, int differedYear)
    {
        if (IsImpossibleYearValue(year, differedYear)) throw new ArgumentOutOfRangeException("Year for due date cannot be a zero or negativ value");
        _year = year;
        _differedYear = differedYear;
    }

    public int Year { get { return _year; } }

    public int NumberDueDatePerYear { get { return _numberDueDatePerYear; } }

    public int GetNumberOfDueDate() { return _numberDueDatePerYear * _year; }

    public int GetNumberDueDateInOneYear() { return _numberDueDatePerYear; }

    public int GetNumberDueDateDiffered()
    {
        if (_differedYear >= _year) return GetNumberOfDueDate() - 1;
        else return _differedYear * _numberDueDatePerYear;
    }
    public int GetNumberOfAmortization() { return GetNumberOfDueDate() - GetNumberDueDateDiffered(); }

    private static bool IsImpossibleYearValue(int year, int differedYear) { return year <= 0 || differedYear < 0; }
}
