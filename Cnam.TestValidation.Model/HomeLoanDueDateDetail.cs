﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.Model;
public class HomeLoanDueDateDetail
{
    private int _dueDateNumber;
    private Money _remainingToPay;
    private Money _interest;
    private Money _principale;
    private Money _assurance;
    private Money _echeance;

    public int DueDateNumber { get { return _dueDateNumber; } }
    public Money remainingToPay { get { return _remainingToPay; } }
    public Money Interest { get { return _interest; } }
    public Money Principale { get { return _principale; } }
    public Money Assurance { get { return _assurance; } }
    public Money TotalCostAtDueDate { get { return _echeance; } }
    public HomeLoanDueDateDetail(int dueDateNumber, Money remainingToPay, Money interest, Money principale, Money assurance, Money echeance)
    {
        _dueDateNumber = dueDateNumber;
        _remainingToPay = remainingToPay;
        _interest = interest;
        _principale = principale;
        _assurance = assurance;
        _echeance = echeance;
    }
}
