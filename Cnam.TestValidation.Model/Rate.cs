﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.Model;
public enum TypeRate : byte
{
    StandardNumber,
    Percent
}
public class Rate
{
    private double _value;
    private TypeRate _typeOfRate;

    public double Value { get { return _value; } }
    public TypeRate Type { get { return _typeOfRate; } }

    public Rate(double value, TypeRate typeRate)
    {
        _value = value;
        _typeOfRate = typeRate;
    }
    public bool IsPercent() { return _typeOfRate == TypeRate.Percent; }
    public bool IsStandardNumber() { return _typeOfRate == TypeRate.StandardNumber; }

    public double GetValueAsPercent()
    {
        if (_typeOfRate == TypeRate.Percent) return _value;
        else if (_typeOfRate == TypeRate.StandardNumber) return _value / 100d;
        else throw new NullReferenceException("Rate neither percent or number");
    }

    public double GetValueAsStandardNumber()
    {
        if (_typeOfRate == TypeRate.StandardNumber) return _value;
        else if (_typeOfRate == TypeRate.Percent) return _value / 100d;
        else throw new NullReferenceException("Rate neither percent or number");
    }

    public void SwitchRateTypeNumber()
    {
        if (_typeOfRate == TypeRate.StandardNumber)
        {
            _typeOfRate = TypeRate.Percent;
            _value = _value * 100d;
        }
        else if (_typeOfRate == TypeRate.Percent)
        {
            _typeOfRate = TypeRate.Percent;
            _value = _value / 100d;
        }
        else throw new NullReferenceException("Rate neither percent or number");
    }
}