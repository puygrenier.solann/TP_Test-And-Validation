﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.Model;
public class HomeLoanRequestBuilder
{

    private char _currency;
    private TypeRate _typeRate;
    private decimal _requestedMoney;
    private int _requestedYear;
    private int _differedYear;
    private float _interestRateValue;
    private float _assuranceRateValue;

    public HomeLoanRequestBuilder()
    {
        _differedYear = 0;
        _typeRate = TypeRate.StandardNumber;
    }

    public void Reset()
    {
        _currency = ' ';
        _requestedMoney = 0;
    }

    public char Currency { get { return _currency; } set { _currency = value; } }

    public void SetRateInStandardNumber()
    {
        _typeRate = TypeRate.StandardNumber;
    }

    public void SetRateInPercent()
    {
        _typeRate = TypeRate.Percent;
    }
    public decimal RequestedMoney { get { return _requestedMoney; } set { _requestedMoney = value; } }

    public int RequestedYear { get { return _requestedYear; } set { _requestedYear = value; } }
    public int DifferedYear { get { return _differedYear; } set { _differedYear = value; } }

    public float InterestRate { get { return _interestRateValue; } set { _interestRateValue = value; } }

    public float AssuranceRate { get { return _assuranceRateValue; } set { _assuranceRateValue = value; } }

    public bool CanBeInstantiate()
    {
        List<char> listCorrectCurrency = new List<char> { '€', '$', '£' };
        if (!listCorrectCurrency.Contains(_currency)) return false;
        if (ContainNegativValue()) return false;
        return true;
    }

    private bool ContainNegativValue()
    {
        return _requestedMoney < 0 || _requestedYear < 0 || _differedYear < 0 || _interestRateValue < 0 || _assuranceRateValue < 0;
    }

    public HomeLoan Build()
    {
        if (!CanBeInstantiate()) throw new InvalideHomeLoanException();
        return new HomeLoan(
            new Money(_requestedMoney, _currency),
            new Rate(_interestRateValue, _typeRate),
            _requestedYear,
            _differedYear,
            new Rate(_assuranceRateValue, _typeRate));
    }
}
