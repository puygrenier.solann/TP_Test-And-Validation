﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.Model;
[Serializable]
public class InvalideHomeLoanException : Exception
{
    public InvalideHomeLoanException() : base() { }
    public InvalideHomeLoanException(string message) : base(message) { }
    public InvalideHomeLoanException(string message, Exception inner) : base(message, inner) { }
    protected InvalideHomeLoanException(System.Runtime.Serialization.SerializationInfo info,
    System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
}
