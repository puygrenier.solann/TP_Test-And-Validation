﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.Model;
public enum CalculationType : byte
{
    P, // Proportionel
    A  // Actuariel
}
public class PeriodicaleRate
{

    CalculationType calculationMode = CalculationType.P;
    private Rate _rate;
    public PeriodicaleRate(Rate rate)
    {
        _rate = rate;
    }

    public double CalculatePeriodicalRateInStandardNumber() { return Calculation(); }

    public double CalculatePeriodicalRateInPercent() { return Calculation() * 100d; }

    private double Calculation()
    {
        if (calculationMode == CalculationType.P) return _rate.GetValueAsStandardNumber() / 12;
        else return Math.Pow(_rate.GetValueAsStandardNumber() + 1, (1d / 12d)) - 1d;
    }

}
