﻿namespace Cnam.TestValidation.WebApi
{
    public class BodyReplyForHomeLoan
    {
        public decimal global_price_mensuality {  get; set; }

        public decimal amount_mensual_guarantee_dues { get; set; }

        public decimal amount_refund_interest { get; set; }

        public decimal amount_guarantee { get; set; }
    }
}
