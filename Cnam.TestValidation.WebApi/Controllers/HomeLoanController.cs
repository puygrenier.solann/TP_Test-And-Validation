using Microsoft.AspNetCore.Mvc;
using Cnam.TestValidation.Model;
using System.Diagnostics;

namespace Cnam.TestValidation.WebApi.Controllers
{
    
    [ApiController]
    [Route("[controller]")]
    public class HomeLoanController : ControllerBase
    {
        private readonly ILogger<HomeLoanController> _logger;

        public HomeLoanController(ILogger<HomeLoanController> logger)
        {
            _logger = logger;
        }

        [Route("/home_loan/generale_data")]
        [HttpPost]
        public async Task<ActionResult<BodyReplyForHomeLoan>> GetHomeLoanData(BodyRequestForHomeLoan homeLoanRequest)
        {
            Trace.WriteLine("Requested of /home_loan/generale_data");

            HomeLoanRequestBuilder homeLoanBuilder = new HomeLoanRequestBuilder();

            homeLoanBuilder.Currency = homeLoanRequest.requested_money.Currency;

            homeLoanBuilder.RequestedMoney = homeLoanRequest.requested_money.Value;

            homeLoanBuilder.RequestedYear = homeLoanRequest.requested_year;

            homeLoanBuilder.DifferedYear = homeLoanRequest.differed_year;

            homeLoanBuilder.InterestRate = homeLoanRequest.interest_rate;
            homeLoanBuilder.AssuranceRate = homeLoanRequest.assurance_rate;

            if (homeLoanRequest.rate_in_percent) homeLoanBuilder.SetRateInPercent();
            else homeLoanBuilder.SetRateInStandardNumber();

           // if(homeLoanBuilder.CanBeInstantiate()) return StatusCode(StatusCodes.Status500InternalServerError, homeLoanBuilder);
            HomeLoan homeLoan = homeLoanBuilder.Build();

            BodyReplyForHomeLoan replyBody = new BodyReplyForHomeLoan();

            replyBody.global_price_mensuality = homeLoan.GetMonthlyAssuranceCost().Value + homeLoan.GetMonthlyCostWithoutAssurance().Value;

            replyBody.amount_mensual_guarantee_dues  = homeLoan.GetMonthlyAssuranceCost().Value;

            replyBody.amount_refund_interest = homeLoan.GetMonthlyCostWithoutAssurance().Value;

            replyBody.amount_guarantee = homeLoan.GetTotalAssuranceCost().Value;

            return Ok(replyBody);
        }

        [Route("/home_loan/refund_years/{years_refunded}")]
        [HttpPost]
        public async Task<ActionResult<BodyReplyForHomeLoan>> GetRefundLoan([FromRoute]int years_refunded,BodyRequestForHomeLoan homeLoanRequest)
        {
            Trace.WriteLine("Requested of /home_loan/generale_data");

            HomeLoanRequestBuilder homeLoanBuilder = new HomeLoanRequestBuilder();

            homeLoanBuilder.Currency = homeLoanRequest.requested_money.Currency;

            homeLoanBuilder.RequestedMoney = homeLoanRequest.requested_money.Value;

            homeLoanBuilder.RequestedYear = homeLoanRequest.requested_year;

            homeLoanBuilder.DifferedYear = homeLoanRequest.differed_year;

            homeLoanBuilder.InterestRate = homeLoanRequest.interest_rate;
            homeLoanBuilder.AssuranceRate = homeLoanRequest.assurance_rate;

            if (homeLoanRequest.rate_in_percent) homeLoanBuilder.SetRateInPercent();
            else homeLoanBuilder.SetRateInStandardNumber();

            //if (homeLoanBuilder.CanBeInstantiate()) return StatusCode(StatusCodes.Status500InternalServerError, homeLoanBuilder);
            HomeLoan homeLoan = homeLoanBuilder.Build();


            return Ok(homeLoan.GetLoanDataAfterDueDate(years_refunded));
        }
    }
    
}