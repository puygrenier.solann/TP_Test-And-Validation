﻿using Cnam.TestValidation.Model;

namespace Cnam.TestValidation.WebApi
{
    public class BodyRequestForHomeLoan
    {
        public Money requested_money { get; init; }
        public int requested_year { get; init; }
        public int differed_year { get; init; }
        public float interest_rate { get; init; }
        public float assurance_rate { get; init; }
        public bool rate_in_percent {  get; init; }
    }
}
